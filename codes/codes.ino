//We always have to include the library
#include "LedControl.h"

/*
 Now we need a LedControl to work with.
 ***** These pin numbers will probably not work with your hardware *****
 pin 12 is connected to the DataIn 
 pin 11 is connected to the CLK 
 pin 10 is connected to LOAD 
 We have only a single MAX72XX.
 */
LedControl  lc = LedControl(12,11,10,1);

int tempo_troca = 1000; //time for change draw and letter | tempo em milesimo para troca de desenho/letra
int level = 0; //letter/draw level control | nivel para controle de passagem
int buttonState = 0; //estado do botão
bool bnt_down=false;
void setup() {

  lc.shutdown(0,false);
  /* Set the brightness to a medium values */
  lc.setIntensity(0,0.5);
  /* and clear the display */
  lc.clearDisplay(0);

  //pin do botao
  pinMode(2, INPUT);
  pinMode(4,OUTPUT);
  digitalWrite(4,HIGH);
  
}

/**
 * Função que mostra as letras
 * function that show the letters
 */
void mostra_letra(byte letra[8]) { //show letter
  for (int i=0;i<8; i=i+1) {
      lc.setRow(0, i, letra[i]);
   }
   delay(tempo_troca);
   lc.clearDisplay(0);
}

void mostra_desenho(byte letra[8]) { //show draw

  for (int i=0;i<8; i=i+1) {
      lc.setRow(0, i, letra[i]);
   }
   delay(tempo_troca);
   lc.clearDisplay(0);

}



