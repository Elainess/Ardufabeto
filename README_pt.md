# Sobre

Este projeto foi criado para ensinar o alfabeto para crianças.

# O que você precisa

- Arduino
- botão
- Matriz 8x8 com controlador MAX7219
- Cabos Jumpers

# Como isso funciona

Depois de carregar o codigo no Arduino com os componentes plugados,
O Aluno irá apertar o único botão e mudar a letra e desenho relativos.
 
# Objetivo

Fazer a criança aprender o alfaberto e desenvolver o pensamento lógico.


# Code

O código do projeto está separado em mais de um arquivo para facilitar a leitura e alteraçã.
